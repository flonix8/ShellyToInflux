# Shelly To Influx
Reads info about your shelly plug and posts it to InfluxDB.

![grafana dashboard with two shellyplug s added to it](documentation/grafana-example.png)


## Setup (Telegraf)

Use `shellyplug-meter.conf` with Telegraf, just put it into your `/etc/telegraf/telegraf.d` directory and insert your shellyplug URLs in the `url` section. Make sure to keep the `/status/0` part at the end.

Telegraf will post the measurements under `shellies` which was chosen because this is the canonical name for the mqtt endpoints. You can change this in `shellyplug-meter.conf`.


## Setup (Grafana)

You may also use the `shellyplug-dashboard.json` with Grafana, just import it as a dashboard. While setting up this dashboard, you can set your local energy price in this dashboard's variables:

![grafana variables: kwhPrice](documentation/grafana-variables.png)


## Fields and their units

Sheshellyplug Documentation below for further info. The most odd one must be watt-minute ("total").  The Grafana dashboard correctly converts to the more common "watt hours" (which is just a division by 60, of course). We're keeping almost all of the Shellyplug names, I don't see a reason to change them even though there are inconsistencies.

* **timestamp**: (number) Unix timestamp of the last energy counter value - we use this as timestamp in telegraf, taken from `meters.0.timestamp`. Not itself a field.
* **total**: (number) Total energy consumed by the attached electrical appliance in Watt-minute
* **power**: (number) Current real AC power being drawn, in Watts
* **overpower**: (number) Value in Watts, on which an overpower condition is detected
* **temperature**: (number) Device temperature in °celsius
* **overtemperature**: (bool) true if device has overheated
* **ison**: (bool) Whether the channel is turned ON or OFF
* **is_valid**: (bool) Whether power metering self-checks OK
* **has_update**: (bool) If there is a firmware update available
* **connected**: (bool) If it is connected to the wifi (somewhat moot since we could never receive a "false" on this one.)
* **rssi**: (int) Wifi signal strength in dBm


## Tags

* **mac**: MAC address of the device. Format `AABBCCDDEEFF`, no seperators
* **ssid"**: SSID of the currently connected wifi
* **ip**: Current IP. Format `123.123.123.123`
* **firmware**: Currently running firmware, taken from `update.old_firmware` of the API, renamed for clarity


## References
* https://shelly-api-docs.shelly.cloud/gen1/#shelly-plug-plugs-meter-0
* https://shelly-api-docs.shelly.cloud/gen1/#shelly-plug-plugs-status
* https://shelly-api-docs.shelly.cloud/gen1/#shelly-plug-plugs-relay-0
* https://github.com/influxdata/telegraf/blob/master/plugins/inputs/http/README.md
* https://github.com/influxdata/telegraf/blob/master/plugins/parsers/json_v2/README.md
